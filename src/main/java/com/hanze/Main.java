/***
 * Created by Dylan Hiemstra
 */
package com.hanze;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;

public class Main {
    /**
     * Connection pool. 800 connections
     */
    private final static ThreadPoolExecutor connectionPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(800);

//    private final static ThreadPoolExecutor workerPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);

    public final static BlockingQueue<Object[]> writeQueue = new LinkedBlockingDeque<>();

    public final static LinkedList<Object[]> last30values = new LinkedList<>();

    public static boolean hasOver30Reached = false;

    /***
     * Created by Dylan Hiemstra
     */
    public static void main(String[] args) {
        // Create a log thread
        Thread logThread = new Thread(() -> {
           while (true) {
               try {
                   System.out.printf(
                           "Active Connections: %d Connection Queue Size: %d " +
                                   "Write Queue Size: %d\n",
                           connectionPool.getActiveCount(),
                           connectionPool.getQueue().size(),
                           writeQueue.size()
                   );
                   Thread.sleep(1000);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
           }
        });

        logThread.start();

        // create worker thread
        Worker worker = new Worker();
        worker.setPriority(Thread.MAX_PRIORITY);
        worker.start();

        // Start the server and start accepting connections
        try {
            //localTesting --> port = 7789 else port 80
            ServerSocket server = new ServerSocket(7789);
            Socket socket;

            while(true) {
                socket = server.accept();

                // Add the new connection to the connection pool
                connectionPool.submit(new SocketConnection(socket));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /***
     * Created by Dylan Hiemstra
     */
    public static synchronized void add(Object[] measurement) {
        last30values.addLast(measurement);

        if(Main.hasOver30Reached || last30values.size() > 30) {
            Main.hasOver30Reached = true;
            last30values.removeFirst();
        }
    }
}
