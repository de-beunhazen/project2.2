/***
 * Created by Dylan Hiemstra
 */

package com.hanze;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import java.io.*;
import java.net.Socket;
import java.sql.Array;
import java.util.HashMap;

public class SocketConnection implements Runnable {
    private Socket socket;

    public SocketConnection(Socket socket) {
        this.socket = socket;
    }

    /**
     * Read from the connection
     *
     * Created by Dylan Hiemstra
     */
    @Override
    public void run() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));

            String line;
            int offset = 0;
            boolean inMeasurement = false;
            Object[] measurement = null;

            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();

                if(line.equals("<MEASUREMENT>")) {
                    inMeasurement = true;
                    measurement = new Object[14];
                    continue;
                }

                if(line.equals("</MEASUREMENT>") && measurement != null) {
                    Main.writeQueue.put(measurement);
                    inMeasurement = false;
                    offset = 0;
                    Thread.yield();
                    continue;
                }


                if(!inMeasurement || measurement == null) {
                    continue;
                }

                int endIndex = line.indexOf("</");

                switch (offset) {
                    case 0:
                        // STN
                        measurement[0] = line.substring(5, endIndex);
                        break;
                    case 1:
                        // DATE
                        measurement[1] = line.substring(6, endIndex);
                        break;
                    case 2:
                        // TIME
                        measurement[2] = line.substring(6, endIndex);
                        break;
                    case 3:
                        // TEMP
                        measurement[3] = toFloatValue(line.substring(6, endIndex));
                        break;
                    case 4:
                        // DEWP
                        measurement[4] = toFloatValue(line.substring(6, endIndex));
                        break;
                    case 5:
                        // STP
                        measurement[5] = toFloatValue(line.substring(5, endIndex));
                        break;
                    case 6:
                        // SLP
                        measurement[6] = toFloatValue(line.substring(5, endIndex));
                        break;
                    case 7:
                        // VISIB
                        measurement[7] = toFloatValue(line.substring(7, endIndex));
                        break;
                    case 8:
                        // WDSP
                        measurement[8] = toFloatValue(line.substring(6, endIndex));
                        break;
                    case 9:
                        // PRCP
                        measurement[9] = toFloatValue(line.substring(6, endIndex));
                        break;
                    case 10:
                        // SNDP
                        measurement[10] = toFloatValue(line.substring(6, endIndex));
                        break;
                    case 11:
                        // FRSHTT
                        measurement[11] = line.substring(8, endIndex);
                        break;
                    case 12:
                        // CLDC
                        measurement[12] = toFloatValue(line.substring(6, endIndex));
                        break;
                    case 13:
                        // WNDDIR
                        measurement[13] = toFloatValue(line.substring(8, endIndex));
                        break;
                }

                offset++;
            }

            this.socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * Created by Dylan Hiemstra
     */
    protected float toFloatValue(String value) {
        if(value.equals("")) {
            return -100000000;
        }

        return Float.parseFloat(value);
    }
}
