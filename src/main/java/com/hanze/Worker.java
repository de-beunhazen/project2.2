

package com.hanze;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class Worker extends Thread {
    private HashMap<String, DataOutputStream> openFiles = new HashMap<>();

    /**
     * Created by Lars Caspers and Dylan Hiemstra
     */
    @Override
    public void run() {
        while (true) {
            // ArrayList<Object[]> measurements = new ArrayList<>();
            Object[] measurement;
            int size = 0;

            try {
                measurement = Main.writeQueue.poll();
                if(measurement == null) continue;
                Object[] measurementValidated = validateMeasurement(measurement);
                Main.add(measurementValidated);


                //while ((measurement = Main.writeQueue.poll()) != null) {
                String stn = (String) measurement[0];
                String date = (String) measurement[1];
//                    String key = stn + date + time[0] + ":" + time[1];

                DataOutputStream out;

                if(!openFiles.containsKey(stn)) {
                    //Testing string Lars = /Users/larscaspers/Desktop/PHP shenanigans/WeatherData
                    //Regular /nf/general/weatherData/
                    //String path = "/mnt/ftpShare/weatherData/" + stn + "/" + date + "/" + time[0] + ":" + time[1];
                    // Testing string dylan = /home/dylan/weatherData/

                    // String path = "/home/dylan/weatherData/";
                    //String path = "/Users/larscaspers/Desktop/PHP shenanigans/WeatherData";

                    String path = "/var/nfs/general/WeatherData" + stn;
                    File stnDirectory = new File(path);
                    if(!stnDirectory.exists()) {
                        stnDirectory.mkdirs();
                    }


                    File outputFile = new File(path + "/" + date);

                    out = new DataOutputStream(
                            new BufferedOutputStream(
                                    new FileOutputStream(outputFile, true),
                                    4200
                            )
                    );

                    openFiles.put(stn, out);
                } else {
                    out = openFiles.get(stn);
                }

                // Created by Lars Caspers
                for(int i = 0; i < 14; i++) {
                    switch (i) {
                        case 0://stationID, used for folder name
                            break;
                        case 1://date user for file name
//                            String[] date = ((String) measurementValidated[i]).split("-");
//                            out.writeShort(Short.parseShort(date[0]));//year
////                              out.writeByte(Byte.parseByte(date[0]));
//                            out.writeByte(Byte.parseByte(date[1]));//month
//                            out.writeByte(Byte.parseByte(date[2]));//day
                            break;
                        case 2:
                            String[] time = ((String) measurementValidated[i]).split(":");
                            out.writeByte(Byte.parseByte(time[0]));//hour
                            out.writeByte(Byte.parseByte(time[1]));//minutes
                            out.writeByte(Byte.parseByte(time[2]));//seconds
//                                out.writeBytes((String) measurementValidated[i]);
//                                System.out.print("seconds ");
                            //System.out.println(seconds);
//                                out.writeByte(seconds);
                            break;
                        case 9://fallen snow 2 decimals
                            float y3 = (float) measurementValidated[i] * 100;
                            int y4 = Math.round(y3) & 0xFFFFFFFF;//mask 32 bits, java converts a 0 to an F for some reason.
//                                System.out.print("fallen snow ");
                            out.writeShort(y4);
//                                System.out.println(y4);
//                                out.writeInt(y4);
                            break;
                        case 11://events(FRSHTT), 6 bits long initially (not bytes!)
                            String binaryString = (String) measurementValidated[i];
                            byte b = binaryString.equals("")
                                    ? (byte)  0x00
                                    : Byte.parseByte(binaryString, 2);
                            out.writeByte(b);
                            break;
                        case 13://WNDDIR, windirection with no decimals.
                            int x = Math.round((float) measurementValidated[i]) & 0xFFFFFFFF;
//                                    System.out.print("Winddirection ");
//                                    System.out.println(x);
                            out.writeShort(x);
                            break;
                        default://standard measurement with 1 decimal and negative values(2's complement)
                            float measurementDecimal = (float) measurementValidated[i] * 10;
                            int measurementNoDecimal = Math.round(measurementDecimal) & 0xFFFFFFFF;
                            //Convert to two bytes because weather data on earth doesn't exceed limits
//                                String signedBit = (measurementNoDecimal & 0x10000000) == 0x10000000
//                                        ? "1"
//                                        : "0";
//                                String highByte = String.valueOf(measurementNoDecimal & 0x0000FF00);
//                                String lowByte = String.valueOf(measurementNoDecimal & 0x000000FF);
//                                String twoBytes = highByte + lowByte;
//                                String stringVal = String.valueOf(measurementNoDecimal);
                            out.writeShort(measurementNoDecimal);
//                                System.out.println(i);
//                                System.out.print("string value  ");
//                                System.out.println(stringVal);
//                                System.out.print("comparison int ");
//                                System.out.println(measurementNoDecimal);
//                                out.writeInt(measurementNoDecimal);
                            break;
                    }


                }

                out.flush();
                //   }



//                measurements.forEach(measurement -> {
//                    try {
//                        // measurement = validateMeasurement(measurement);
//                        Main.add(measurement);
//
//
//                        String stn = (String) measurement[0];
//                        String date = (String) measurement[1];
//                        String[] time = ((String) measurement[2]).split(":");
//                        String key = stn + date + time[0] + ":" + time[1];
//
//                        if(!openFiles.containsKey(key)) {
//                            //Testing string Lars = /Users/larscaspers/Desktop/PHP shenanigans/WeatherData
//                            //Regular /mnt/ftpShare/weatherData/
//                            //String path = "/mnt/ftpShare/weatherData/" + stn + "/" + date + "/" + time[0] + ":" + time[1];
//                            // Testing string dylan = /home/dylan/weatherData/
//
//                            String path = "/home/dylan/weatherData/" + Thread.currentThread().getId() + "/" + stn + "/" + date;
//                            File stnDirectory = new File(path);
//                            if(!stnDirectory.exists()) {
//                                stnDirectory.mkdirs();
//                            }
//
//                            File outputFile = new File(path + "/" + time[0] + ":" + time[1]);
//                            DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile, true));
//                            openFiles.put(key, out);
//                        }
//
//                        DataOutputStream out = openFiles.get(key);
//
//                        for(int i = 0; i < 14; i++) {
//                            switch (i) {
//                                case 0://stationID, used for folder name
//                                case 1://date, user for folder name
//                                    break;
//                                case 2://Time, hour and minutes used for folder name, seconds written to file
//                                    byte seconds = Byte.parseByte(time[2]);
//                                    //System.out.print("seconds ");
//                                    //System.out.println(seconds);
//                                    out.writeByte(seconds);
//                                    break;
//                                case 9://fallen snow 2 decimals
//                                    float y3 = (float) measurement[i] * 100;
//                                    int y4 = Math.round(y3) & 0xFFFFFFFF;//mask 32 bits, java converts a 0 to an F for some reason.
//                                    //System.out.print("fallen snow ");
//                                    //System.out.println(y4);
//                                    out.writeInt(y4);
//                                    break;
//                                case 11://events(FRSHTT), 6 bits long initially (not bytes!)
//                                    String binaryString = (String) measurement[i];
//                                    byte b = binaryString.equals("")
//                                            ? (byte)  0x00
//                                            : Byte.parseByte(binaryString, 2);
////                                    System.out.print("events ");
////                                    System.out.println(b);
//                                    out.writeByte(b);
//                                    break;
////                            //todo 12 and 13 in 2 bytes.
//                                case 13://WNDDIR, windirection with no decimals.
//                                    int x = Math.round((float) measurement[i]) & 0xFFFFFFFF;
////                                    System.out.print("Winddirection ");
////                                    System.out.println(x);
//                                    out.writeInt(x);
//                                    break;
//                                default://standard measurement with 1 decimal
//                                    float y = (float) measurement[i] * 10;
//                                    int y2 = Math.round(y) & 0xFFFFFFFF;
////                                    System.out.print("Default case");
////                                    System.out.print(i);
////                                    System.out.print(" value ");
////                                    System.out.println(y2);
//                                    out.writeInt(y2);
//                                    break;
//                            }
//
//                        }
//
//                        out.flush();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                });
//
//                openFiles.values().forEach(openFile -> {
//                    try {
//                        openFile.close();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                });
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Extrapolate value
     *
     * Created by Lars
     *
     * @param index
     * @param last30values
     * @return
     */
    private float extrapolate(int index, LinkedList<Object[]> last30values) {
        float totalDelta = 0;
        int size = last30values.size();

        if(size == 0) return -1;

        for(int i = 0; i < last30values.size(); i++) {
            if(i == size - 1) continue;

            Object[] current = last30values.get(i);
            Object[] next = last30values.get(i + 1);


            totalDelta += (float) current[index] - (float) next[index];
        }


        return totalDelta / size + (float) Main.last30values.getLast()[index];
    }

    /**
     * Created by Lars Caspers
     *
     * @param measurement
     * @return
     */
    private Object[] validateMeasurement(Object[] measurement) {
        LinkedList<Object[]> last30values = (LinkedList<Object[]>) Main.last30values.clone();

        for(int i = 3; i < measurement.length; ++i) {
            if(i == 11) continue;
            float value = (float) measurement[i];

            // Check temp
            if(i == 3) {
                float extrapolateTemp = extrapolate(i, last30values);
                if(difference(extrapolateTemp, value) >= 20) {
                    measurement[i] = extrapolateTemp;
                }
            } else if(value == -100000000){
                float extrapolateTemp = extrapolate(i, last30values);
                measurement[i] = extrapolateTemp;
            }
        }

        return measurement;
    }

    /**
     * Created by Lars Caspers
     *
     * @return
     */
    private float difference(float a, float b) {
        return Math.abs(a - b) / ((a + b) / 2) * 100;
    }
}
